#include <stdio.h>
#include <string.h>
#include <malloc.h>


char * gimmeAString(){
	// we are going to return "yelhai", which is an array of 6 chars, each at 4 bytes
	// however, it has one hidden char, '\0', which is to c strings as the period is to english
	//
	// sizeof(char) == 4, but if that ever somehow changes, this will ensure stuff still works :D
	char * retval = malloc(sizeof(char) * 7); // give us 4*7 bytes of memory
	strcpy(retval,"yelhai");
	return retval;
}

// standard line giving us number of args and an array of them
int main(int argc, char * argv){
	char * ourString = gimmeAString();

	printf("We got back %s!\n", ourString);

	// this line is extremely important any time you use malloc!
	free(ourString); // give back the memory we borrowed!

	return 0;
}

